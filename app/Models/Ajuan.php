<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ajuan extends Model
{
    public $timestamps = false;
    public $table = 'ajuans';
    public $fillable = ['pejabat_id', 'user_id', 'tanggal', 'waktu', 'tujuan_kunjungan', 'status', 'sifat'];

    public function user()
    {
    	return $this->belongsTo('App\Models\User', 'user_id','id');
    }

    public function pejabat()
    {
    	return $this->belongsTo('App\Models\Pejabat', 'pejabat_id','id');
    }
}
