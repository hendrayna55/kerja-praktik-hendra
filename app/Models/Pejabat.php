<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pejabat extends Model
{
    protected $fillable = ['nama', 'jenis_kelamin', 'jabatan', 'tempat_lahir', 'tanggal_lahir'];

    public function ajuan()
    {
        return $this->hasMany('App\Models\Ajuan', 'pejabat_id');
    }

}
