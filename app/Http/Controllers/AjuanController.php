<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Pejabat;
use App\Models\Ajuan;
use Auth;

class AjuanController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pejabats = Pejabat::all();
        $ajuans = Ajuan::all();
        return view('ajuan.index', compact('ajuans', 'pejabats'));
    }

    public function showKalender()
    {
        $pejabats = Pejabat::all();
        $ajuans = Ajuan::where('status', 'Disetujui')->get();
        return view('jadwal.index', compact('ajuans', 'pejabats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pejabats = Pejabat::get();
        $users = User::first();
        return view('ajuan.create', compact('pejabats', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tanggal' => 'required',
            'waktu' => 'required',
            'pejabat_id' => 'required',
            'tujuan_kunjungan' => 'required',
            'sifat' => 'required',
        ]);

        $status = "Belum Disetujui";
        
        Ajuan::create([
            'user_id' => Auth::user()->id,
            'tanggal' => $request->tanggal,
            'waktu' => $request->waktu,
            'pejabat_id' => $request->pejabat_id,
            'tujuan_kunjungan' => $request->tujuan_kunjungan,
            'sifat' => $request->sifat,
            'status' => $status
        ]);

        return redirect('/ajuan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tanggal' => 'required',
            'waktu' => 'required',
            'tujuan_kunjungan' => 'required',
            'sifat' => 'required',
        ]);
        
        Ajuan::find($id)->update([
            'tanggal' => $request->tanggal,
            'waktu' => $request->waktu,
            'pejabat_id' => $request->pejabat_id,
            'tujuan_kunjungan' => $request->tujuan_kunjungan,
            'sifat' => $request->sifat,
            'status' => $request->status,
        ]);

        return redirect('/ajuan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ajuan::find($id)->delete();
        return redirect('/ajuan');
    }
}
