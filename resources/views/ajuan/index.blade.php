<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajuan</title>
    <link rel="icon" href="https://i.ibb.co/Yp4tr14/isteq-logo-remake.png">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/fontawesome-free/css/all.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets')}}/dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <div class="wrapper">

        <!-- Preloader -->
        <!-- <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
        </div> -->

        @include('layouts.admin.navbar')
        @include('layouts.admin.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Data Pengajuan Kunjungan</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
                                <li class="breadcrumb-item active">Ajuan</li>
                                <li class="breadcrumb-item active">Tabel Pengajuan</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header bg-success">
                                    <h3 class="card-title">Pengaju Kunjungan</h3>
                                    <a href="{{url('/ajuan/tambah')}}" class="btn btn-sm btn-primary float-right">
                                        <i class="fas fa-plus"></i> Buat ajuan
                                    </a>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    
                                    <table id="example1" class="table table-bordered table-hover table-responsive">
                                        <thead class="bg-info">
                                            <tr>
                                                <th class="text-center align-middle">Pengaju</th>
                                                <th class="text-center align-middle">Tanggal</th>
                                                <th class="text-center align-middle">Waktu</th>
                                                <th class="text-center align-middle">Tujuan Pejabat</th>
                                                <th class="text-center align-middle" style="width:30%;">Tujuan Kunjungan</th>
                                                <th class="text-center align-middle">Sifat</th>
                                                <th class="text-center align-middle"style="width:5%;">Status</th>
                                                <th class="text-center align-middle"style="width:7%;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($ajuans as $ajuan)
                                                @if(Auth::user()->admin == true)
                                                <tr>
                                                    <td class="align-middle">{{$ajuan->user->nama}}</td>
                                                    <td class="text-center align-middle">{{$ajuan->tanggal}}</td>
                                                    <td class="text-center align-middle">{{$ajuan->waktu}}</td>
                                                    <td class="text-center align-middle">{{$ajuan->pejabat->nama}}</td>
                                                    <td class="align-middle">{{$ajuan->tujuan_kunjungan}}</td>
                                                    <td class="text-center align-middle">{{$ajuan->sifat}}</td>
                                                    <td class="text-center align-middle">
                                                        @if($ajuan->status == "Belum Disetujui")
                                                            <div class="text-warning">Belum Disetujui</div>
                                                        @elseif($ajuan->status == "Disetujui")
                                                            <div class="text-success">Disetujui</div>
                                                        @else
                                                            <div class="text-danger">Ditolak</div>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <div class="row justify-content-center">
                                                            <div class="btn-sm btn" data-toggle="modal" data-target="#modal-edit{{$ajuan->id}}"><i class="fas fa-edit"></i></div>
                                                            <div class="btn-sm btn" data-toggle="modal" data-target="#modal-hapus{{$ajuan->id}}"><i class="fas fa-trash-alt"></i></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @else
                                                    @if($ajuan->user_id == Auth::user()->id)
                                                    <tr>
                                                        <td class="align-middle">{{$ajuan->user->nama}}</td>
                                                        <td class="text-center align-middle">{{$ajuan->tanggal}}</td>
                                                        <td class="text-center align-middle">{{$ajuan->waktu}}</td>
                                                        <td class="text-center align-middle">{{$ajuan->pejabat->nama}}</td>
                                                        <td class="align-middle">{{$ajuan->tujuan_kunjungan}}</td>
                                                        <td class="text-center align-middle">{{$ajuan->sifat}}</td>
                                                        <td class="text-center align-middle">
                                                            @if($ajuan->status == "Belum Disetujui")
                                                                <div class="text-warning">Belum Disetujui</div>
                                                            @elseif($ajuan->status == "Disetujui")
                                                                <div class="text-success">Disetujui</div>
                                                            @else
                                                                <div class="text-danger">Ditolak</div>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="row justify-content-center">
                                                                <div class="btn-sm btn">
                                                                    @if($ajuan->status == "Disetujui")
                                                                    <i class="fas fa-eye" data-toggle="modal" data-target="#modal-edit"></i>
                                                                    @else
                                                                    <i class="fas fa-edit" data-toggle="modal" data-target="#modal-edit"></i>
                                                                    <div class="btn-sm btn" data-toggle="modal" data-target="#modal-hapus{{$ajuan->id}}"><i class="fas fa-trash-alt"></i></div>
                                                                    @endif
                                                                </div>
                                                                
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                @endif

                                            <!-- Modal Edit -->
                                            <div class="modal fade" id="modal-edit{{$ajuan->id}}">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <form class="form-horizontal text-sm" action="{{url('/ajuan/' . $ajuan->id)}}" method="post">
                                                            <div class="modal-header bg-info">
                                                                @if($ajuan->status == "Disetujui")
                                                                    <h4 class="modal-title">Lihat Data</h4>
                                                                @else
                                                                    <h4 class="modal-title">Edit Data</h4>
                                                                @endif
                                                                <!-- <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button> -->
                                                            </div>
                                                            <div class="modal-body">
                                                                
                                                                @csrf
                                                                @method('PUT')
                                                                
                                                                @if(Auth::user()->admin == true)
                                                                    <div class="form-group row">
                                                                        <label for="status" class="col-sm-12 col-lg-3 col-form-label">Status</label>
                                                                        <div class="col-sm-12 col-lg-8">
                                                                            <select name="status" class="form-control">
                                                                                <option <?php if($ajuan->status == "Belum Disetujui") echo 'selected="selected"' ?>>Belum Disetujui</option>
                                                                                <option <?php if($ajuan->status == "Disetujui") echo 'selected="selected"' ?>>Disetujui</option>
                                                                                <option <?php if($ajuan->status == "Ditolak") echo 'selected="selected"' ?>>Ditolak</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="pejabat_id" class="col-sm-12 col-lg-3 col-form-label">Tujuan Pejabat</label>
                                                                        <div class="col-sm-12 col-lg-8">
                                                                            <select name="pejabat_id" class="form-control">
                                                                                @foreach($pejabats as $pejabat)
                                                                                    <option value="{{$pejabat->id}}" <?php if($ajuan->pejabat_id == $pejabat->id) echo 'selected="selected"' ?>>{{$pejabat->nama}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="tanggal" class="col-sm-12 col-lg-3 col-form-label">Tanggal Kunjungan</label>
                                                                        <div class="col-sm-12 col-lg-8">
                                                                            <input type="date" class="form-control" id="tanggal" name="tanggal" placeholder="Masukkan Tempat Lahir Mabigus" value="{{old('tanggal') ? old('tanggal'):$ajuan->tanggal}}" required>
                                                                            @error('tanggal')
                                                                                <p class="text-danger text-sm">{{$message}}</p>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="waktu" class="col-sm-12 col-lg-3 col-form-label">Waktu Kunjungan</label>
                                                                        <div class="col-sm-12 col-lg-8">
                                                                            <input type="time" class="form-control" id="waktu" name="waktu" value="{{old('waktu') ? old('waktu'):$ajuan->waktu}}" required>
                                                                            @error('waktu')
                                                                                <p class="text-danger text-sm">{{$message}}</p>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="sifat" class="col-sm-12 col-lg-3 col-form-label">Sifat Kunjungan</label>
                                                                        <div class="col-sm-12 col-lg-8">
                                                                            <select name="sifat" class="form-control">
                                                                                <option <?php if($ajuan->status == "Biasa") echo 'selected="selected"' ?>>Biasa</option>
                                                                                <option <?php if($ajuan->status == "Penting") echo 'selected="selected"' ?>>Penting</option>
                                                                                <option <?php if($ajuan->status == "Sangat Penting") echo 'selected="selected"' ?>>Sangat Penting</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="tujuan_kunjungan" class="col-sm-12 col-lg-3 col-form-label">Tujuan Kunjungan</label>
                                                                        <div class="col-sm-12 col-lg-8">
                                                                            <textarea type="text" class="form-control" id="tujuan_kunjungan" name="tujuan_kunjungan" placeholder="Masukkan Tujuan Kunjungan" rows="5" required>{{old('tujuan_kunjungan') ? old('tujuan_kunjungan'):$ajuan->tujuan_kunjungan}}</textarea>
                                                                            @error('tujuan_kunjungan')
                                                                                <p class="text-danger text-sm">{{$message}}</p>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                @elseif(Auth::user()->admin == false)
                                                                    <div class="form-group row">
                                                                        <label for="status" class="col-sm-12 col-lg-3 col-form-label">Status</label>
                                                                        <div class="col-sm-12 col-lg-8">
                                                                            <input type="text" class="form-control" id="status" name="status" value="{{old('status') ? old('status'):$ajuan->status}}" required readonly>
                                                                            @error('status')
                                                                                <p class="text-danger text-sm">{{$message}}</p>
                                                                            @enderror
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="pejabat_id" class="col-sm-12 col-lg-3 col-form-label">Tujuan Pejabat</label>
                                                                        <div class="col-sm-12 col-lg-8">
                                                                            @if($ajuan->status == "Disetujui")
                                                                                <input type="text" class="form-control" id="pejabat_id" name="pejabat_id" value="{{old('pejabat_id') ? old('pejabat_id'):$ajuan->pejabat->nama}}" required readonly>
                                                                                @error('pejabat_id')
                                                                                    <p class="text-danger text-sm">{{$message}}</p>
                                                                                @enderror
                                                                            @else
                                                                                <select name="pejabat_id" class="form-control">
                                                                                    @foreach($pejabats as $pejabat)
                                                                                        <option value="{{$pejabat->id}}" <?php if($ajuan->pejabat_id == $pejabat->id) echo 'selected="selected"' ?>>{{$pejabat->nama}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label for="tanggal" class="col-sm-12 col-lg-3 col-form-label">Tanggal Kunjungan</label>
                                                                        <div class="col-sm-12 col-lg-8">
                                                                            @if($ajuan->status == "Disetujui")
                                                                                <input type="date" class="form-control" id="tanggal" name="tanggal" value="{{old('tanggal') ? old('tanggal'):$ajuan->tanggal}}" required readonly>
                                                                                @error('tanggal')
                                                                                    <p class="text-danger text-sm">{{$message}}</p>
                                                                                @enderror
                                                                            @else
                                                                                <input type="date" class="form-control" id="tanggal" name="tanggal" value="{{old('tanggal') ? old('tanggal'):$ajuan->tanggal}}" required>
                                                                                @error('tanggal')
                                                                                    <p class="text-danger text-sm">{{$message}}</p>
                                                                                @enderror
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="waktu" class="col-sm-12 col-lg-3 col-form-label">Waktu Kunjungan</label>
                                                                        <div class="col-sm-12 col-lg-8">
                                                                            @if($ajuan->status == "Disetujui")
                                                                                <input type="time" class="form-control" id="waktu" name="waktu" value="{{old('waktu') ? old('waktu'):$ajuan->waktu}}" required readonly>
                                                                                @error('waktu')
                                                                                    <p class="text-danger text-sm">{{$message}}</p>
                                                                                @enderror
                                                                            @else
                                                                                <input type="time" class="form-control" id="waktu" name="waktu" value="{{old('waktu') ? old('waktu'):$ajuan->waktu}}" required>
                                                                                @error('waktu')
                                                                                    <p class="text-danger text-sm">{{$message}}</p>
                                                                                @enderror
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="sifat" class="col-sm-12 col-lg-3 col-form-label">Sifat Kunjungan</label>
                                                                        <div class="col-sm-12 col-lg-8">
                                                                            @if($ajuan->status == "Disetujui")
                                                                                <input type="text" class="form-control" id="sifat" name="sifat" value="{{old('sifat') ? old('sifat'):$ajuan->sifat}}" required readonly>
                                                                                @error('sifat')
                                                                                    <p class="text-danger text-sm">{{$message}}</p>
                                                                                @enderror
                                                                            @else
                                                                                <select name="sifat" class="form-control">
                                                                                    <option <?php if($ajuan->sifat == "Biasa") echo 'selected="selected"' ?>>Biasa</option>
                                                                                    <option <?php if($ajuan->sifat == "Penting") echo 'selected="selected"' ?>>Penting</option>
                                                                                    <option <?php if($ajuan->sifat == "Sangat Penting") echo 'selected="selected"' ?>>Sangat Penting</option>
                                                                                </select>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label for="tujuan_kunjungan" class="col-sm-12 col-lg-3 col-form-label">Tujuan Kunjungan</label>
                                                                        <div class="col-sm-12 col-lg-8">
                                                                            @if($ajuan->status == "Disetujui")
                                                                                <textarea type="text" class="form-control" id="tujuan_kunjungan" name="tujuan_kunjungan" placeholder="Masukkan Tujuan Kunjungan" rows="5" required readonly>{{old('tujuan_kunjungan') ? old('tujuan_kunjungan'):$ajuan->tujuan_kunjungan}}</textarea>
                                                                                @error('tujuan_kunjungan')
                                                                                    <p class="text-danger text-sm">{{$message}}</p>
                                                                                @enderror
                                                                            @else
                                                                                <textarea type="text" class="form-control" id="tujuan_kunjungan" name="tujuan_kunjungan" placeholder="Masukkan Tujuan Kunjungan" rows="5" required>{{old('tujuan_kunjungan') ? old('tujuan_kunjungan'):$ajuan->tujuan_kunjungan}}</textarea>
                                                                                @error('tujuan_kunjungan')
                                                                                    <p class="text-danger text-sm">{{$message}}</p>
                                                                                @enderror
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                @endif                                                           
                                                            </div>
                                                            <div class="modal-footer justify-content-between">
                                                                @if(Auth::user()->admin == true)
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>

                                                                    <button type="submit" class="btn btn-success btn-sm mr-2" data-toggle="modal" data-target="#modal-default">Simpan</button>
                                                                @else
                                                                    @if($ajuan->status == "Disetujui")
                                                                        <button type="button" class="btn btn-default float-right" data-dismiss="modal">Tutup</button>
                                                                    @else
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                                        
                                                                        <button type="submit" class="btn btn-success btn-sm mr-2" data-toggle="modal" data-target="#modal-default">Simpan</button>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal Edit -->

                                            <!-- Modal Hapus -->
                                            <div class="modal fade" id="modal-hapus{{$ajuan->id}}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header bg-info">
                                                            <h4 class="modal-title">Hapus Data Ajuan</h4>
                                                            <!-- <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button> -->
                                                        </div>
                                                        <div class="modal-body">
                                                            <p class="">
                                                                Pengaju : {{$ajuan->user->nama}}<br>
                                                                Tujuan Pejabat : {{$ajuan->pejabat->nama}}<br>
                                                                Tujuan Kunjungan : {{$ajuan->tujuan_kunjungan}}<br><br>
                                                                Apakah anda yakin untuk menghapus data ini?
                                                            </p>
                                                        </div>
                                                        <div class="modal-footer justify-content-between">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                            <form action="{{url('/ajuan/' . $ajuan->id)}}" method="post" class="">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button class="btn btn-danger btn-sm mr-2" data-toggle="modal" data-target="#modal-hapus">Hapus</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @include('layouts.admin.footer')

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- Wrapper -->

    <!-- jQuery -->
    <script src="{{asset('assets')}}/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('assets')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{asset('assets')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{asset('assets')}}/plugins/jszip/jszip.min.js"></script>
    <script src="{{asset('assets')}}/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{asset('assets')}}/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('assets')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('assets')}}/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('assets')}}/dist/js/demo.js"></script>
    <!-- Page specific script -->
    <script>
    $(function () {
        $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        });
    });
    </script>
</body>
</html>