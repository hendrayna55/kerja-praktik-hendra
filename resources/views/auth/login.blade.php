<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login E-Isteq</title>

    @include('layouts.head')

    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('assets')}}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <img src="https://i.ibb.co/Yp4tr14/isteq-logo-remake.png" alt="isteq-logo" style="height:50px;"><b> E-</b>Isteq
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Login untuk memulai sesi</p>

                <form action="{{ route('login') }}" method="post">
                    @csrf

                    <div class="input-group mb-3">
                        <input id="credential" type="text" class="form-control @error('credential') is-invalid @enderror" name="credential" value="{{ old('credential') }}" required autocomplete="credential" placeholder="Username/Email" autofocus>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="input-group mb-3">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="row">
                        <!-- <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" id="remember">
                                <label for="remember">
                                    Remember Me
                                </label>
                            </div>
                        </div> -->
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Login</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

                <p class="mb-1">
                <a href="{{ route('password.request') }}">Lupa Password</a>
                </p>
                <p class="mb-0">
                <a href="{{ route('register') }}" class="text-center">Daftar Pengguna Baru</a>
                </p>
            </div>
            <!-- /.login-card-body -->
        </div>
  </div>
  <!-- /.login-box -->
</body>
</html>