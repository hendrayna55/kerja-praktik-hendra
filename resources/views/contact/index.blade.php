@extends('layouts.admin.adminapp')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Narahubung</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
                        <li class="breadcrumb-item active"><i class="fas fa-headset"></i> Narahubung</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card card-success">
                        <div class="card-header">
                            <h3 class="card-title">Call Center / Narahubung</h3>
                            @auth
                                @if(Auth::user()->admin == true)
                                <a href="{{url('/contact/tambah')}}" class="btn btn-sm btn-primary float-right">
                                    <i class="fas fa-plus"></i> Tambah Kontak
                                </a>
                                @endif
                            @endauth
                        </div>
        
                        <div class="card-body">
                            <table class="table table-borderless table-sm">
                                <tbody>
                                    @foreach($contacts as $contact)
                                    <tr>
                                        <td class="col-4">{{$contact->nama_kontak}}</td>
                                        <td>:</td>
                                        @if($contact->link_kontak == null)
                                            <td>{{$contact->kontak}}</td>
                                        @else
                                            <td><a href="{{$contact->link_kontak}}" target="_blank">{{$contact->kontak}}</a></td>
                                        @endif

                                        @auth
                                            @if(Auth::user()->admin == true)
                                            <td class="row justify-content-center">
                                                <div class="btn btn-sm text-success" data-toggle="modal" data-target="#modal-edit{{$contact->id}}">
                                                    <i class="fas fa-edit"></i>
                                                </div>
                                                <div class="btn btn-sm text-danger" data-toggle="modal" data-target="#modal-hapus{{$contact->id}}">
                                                    <i class="fas fa-trash-alt"></i>
                                                </div>
                                            </td>
                                            @endif
                                        @endauth
                                    </tr>
                                    <!-- Modal Edit -->
                                    <div class="modal fade" id="modal-edit{{$contact->id}}">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <form class="form-horizontal text-sm" action="{{url('/contact/' . $contact->id)}}" method="post" id="">
                                                    @csrf
                                                    @method('put')
                                                    <div class="modal-header bg-info">
                                                        <h4 class="modal-title">Edit Data Narahubung</h4>
                                                        <!-- <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button> -->
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <label for="nama_kontak" class="col-sm-4 col-lg-3 col-form-label">Nama Kontak</label>
                                                            <div class="col-sm-8 col-lg-9">
                                                                <input type="text" class="form-control text-sm" id="nama_kontak" name="nama_kontak" value="{{old('nama_kontak')?old('nama_kontak'):$contact->nama_kontak}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="kontak" class="col-sm-4 col-lg-3 col-form-label">Nomor / Username</label>
                                                            <div class="col-sm-8 col-lg-9">
                                                                <input type="text" class="form-control text-sm" id="kontak" name="kontak" value="{{old('kontak')?old('kontak'):$contact->kontak}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="link_kontak" class="col-sm-4 col-lg-3 col-form-label">Link</label>
                                                            <div class="col-sm-8 col-lg-9">
                                                                <input type="text" class="form-control text-sm" id="link_kontak" name="link_kontak" value="{{old('link_kontak')?old('link_kontak'):$contact->link_kontak}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                        <button type="submit" class="btn btn-success btn-sm mr-2" data-toggle="modal" data-target="#modal-hapus">Simpan</button>
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->

                                    <!-- Modal Hapus -->
                                    <div class="modal fade" id="modal-hapus{{$contact->id}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header bg-info">
                                                    <h4 class="modal-title">Hapus Data Narahubung</h4>
                                                    <!-- <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button> -->
                                                </div>
                                                <div class="modal-body">
                                                    <p class="">
                                                        Apakah anda yakin untuk menghapus data ini?
                                                    </p>
                                                </div>
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                    <form action="{{url('/contact/' . $contact->id)}}" method="post" class="">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger btn-sm mr-2" data-toggle="modal" data-target="#modal-hapus">Hapus</button>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
@endsection
