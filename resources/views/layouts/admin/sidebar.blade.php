<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Brand Logo -->
    <a href="{{url('/dashboard')}}" class="brand-link">
        <img src="https://i.ibb.co/Yp4tr14/isteq-logo-remake.png" alt="isteq-logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">
            Admin E-Isteq
        </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('assets/dist/img/logo-user-2.png')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">
                    @if (Route::has('login'))
                        @auth
                            {{Auth::user()->nama}}
                        @else
                            <p>Guest</p>
                        @endauth
                    @endif
                    
                </a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            @if (Route::has('login'))
                @auth
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                        
                        <li class="nav-item">
                            <a href="{{url('/dashboard')}}" class="nav-link">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('/pejabat')}}" class="nav-link">
                                <i class="nav-icon fas fa-list"></i>
                                <p>
                                    Daftar Pejabat
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('/jadwal')}}" class="nav-link">
                                <i class="nav-icon fas fa-calendar-alt"></i>
                                <p>
                                    Jadwal
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('/ajuan')}}" class="nav-link">
                                <i class="nav-icon fas fa-clipboard-list"></i>
                                <p>
                                    Pengajuan Kunjungan
                                </p>
                            </a>
                        </li>

                        <!-- <li class="nav-item">
                            <a href="{{url('/induk/profil')}}" class="nav-link">
                                <i class="nav-icon fas fa-user-circle"></i>
                                <p>
                                    Profil
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-cog"></i>
                                <p>
                                    Akun
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>

                            <ul class="nav nav-treeview text-sm">

                                <li class="nav-item">
                                    <a href="{{url('/akun/biodata')}}" class="nav-link">
                                    <i class="nav-icon fas fa-building"></i>
                                        <p>Data Pangkalan</p>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="{{url('/akun/ubahpassword')}}" class="nav-link">
                                        <i class="nav-icon fas fa-lock"></i>
                                        <p>Ubah Password</p>
                                    </a>
                                </li>
                            </ul>
                        </li> -->

                        <li class="nav-item">
                            <a href="{{url('/contact')}}" class="nav-link">
                                <i class="nav-icon fas fa-headset"></i>
                                <p>
                                    Narahubung
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="nav-link">
                                <i class="nav-icon fas fa-sign-out-alt"></i>
                                <p>
                                    Sign Out
                                </p>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </a>
                        </li>
                    </ul>
                @else
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item">
                            <a href="{{url('/')}}" class="nav-link">
                                <i class="nav-icon fas fa-home"></i>
                                <p>
                                    Halaman Utama
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('/login')}}" class="nav-link">
                                <i class="nav-icon fas fa-sign-in-alt"></i>
                                <p>
                                    Login
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{url('/register')}}" class="nav-link">
                            <i class="nav-icon fas fa-user-plus"></i>
                                <p>
                                    Daftar
                                </p>
                            </a>
                        </li>
                    </ul>
                @endauth
            @endif
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->

</aside>