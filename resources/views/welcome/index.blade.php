<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>E-Isteq</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link rel="icon" href="https://i.ibb.co/Yp4tr14/isteq-logo-remake.png">
  <link href="{{asset('assetswelcome')}}/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('assetswelcome')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{asset('assetswelcome')}}/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{asset('assetswelcome')}}/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{asset('assetswelcome')}}/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{asset('assetswelcome')}}/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="{{asset('assetswelcome')}}/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('assetswelcome')}}/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container-fluid">

      <div class="row justify-content-center">
        <div class="col-xl-9 d-flex align-items-center justify-content-lg-between">
          <!-- <a href="index.html" class="logo me-auto me-lg-0"><img src="https://i.ibb.co/Yp4tr14/isteq-logo-remake.png" alt="" class="img-fluid"></a> -->
          <h1 class="logo me-auto me-lg-0"><a href="index.html">m-Isteq</a></h1>
          <!-- Uncomment below if you prefer to use an image logo -->

          <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>
              <li><a class="nav-link " href="{{('/pejabat')}}">Daftar Pejabat</a></li>
              <li><a class="nav-link " href="{{('/jadwal')}}">Lihat Jadwal</a></li>
              <li><a class="nav-link " href="{{('/contact')}}">Narahubung</a></li>
              <!-- <li><a class="nav-link scrollto" href="#about">About</a></li>
              <li><a class="nav-link scrollto" href="#services">Services</a></li>
              <li><a class="nav-link scrollto " href="#portfolio">Portfolio</a></li>
              <li><a class="nav-link scrollto" href="#pricing">Pricing</a></li>
              <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
                <ul>
                  <li><a href="#">Drop Down 1</a></li>
                  <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
                    <ul>
                      <li><a href="#">Deep Drop Down 1</a></li>
                      <li><a href="#">Deep Drop Down 2</a></li>
                      <li><a href="#">Deep Drop Down 3</a></li>
                      <li><a href="#">Deep Drop Down 4</a></li>
                      <li><a href="#">Deep Drop Down 5</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Drop Down 2</a></li>
                  <li><a href="#">Drop Down 3</a></li>
                  <li><a href="#">Drop Down 4</a></li>
                </ul>
              </li>
              <li><a class="nav-link scrollto" href="#contact">Contact</a></li> -->
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
          </nav>
          <!-- .navbar -->
          <div>
            <a href="{{'/login'}}" class="get-started-btn btn scrollto">Login</a>
            <a href="{{'/register'}}" class="btn btn-danger scrollto">Daftar</a>

          </div>

        </div>
      </div>

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex flex-column justify-content-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-xl-8">
          <h1>Selamat Datang</h1>
          <h2>Di website aplikasi kunjungan YPI Al-Istiqomah</h2>
          <!-- <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox play-btn mb-4"></a> -->
        </div>
      </div>
    </div>
  </section><!-- End Hero -->


  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{asset('assetswelcome')}}/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="{{asset('assetswelcome')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{asset('assetswelcome')}}/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="{{asset('assetswelcome')}}/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="{{asset('assetswelcome')}}/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="{{asset('assetswelcome')}}/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('assetswelcome')}}/js/main.js"></script>

</body>

</html>