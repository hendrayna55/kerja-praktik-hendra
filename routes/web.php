<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\AjuanController;
use App\Http\Controllers\PejabatController;
use App\Http\Controllers\ContactController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome.index');
});
Route::get('/pejabat',[PejabatController::class,'index']);
Route::get('/jadwal',[AjuanController::class,'showKalender']);
Route::get('/contact',[ContactController::class,'index']);

Auth::routes();

// Route::get('/dashboard', 'HomeController@index')->name('home');
Route::middleware(['admin'])->group(function(){
    Route::prefix('admin')->group(function() 
    {
        Route::get('/dashboard',[AdminController::class,'index']);
    });
    Route::prefix('pejabat')->group(function() 
    {
        Route::get('/tambah',[PejabatController::class,'create']);
        Route::post('/',[PejabatController::class,'store']);
        Route::get('/edit/{id}',[PejabatController::class,'edit']);
        Route::put('/{id}',[PejabatController::class,'update']);
        Route::delete('/{id}',[PejabatController::class,'destroy']);
    });

    Route::prefix('ajuan')->group(function() 
    {
        Route::get('/tambah',[AjuanController::class,'create']);
    });

    Route::prefix('contact')->group(function() 
    {
        // Route::get('/',[ContactController::class,'index']);
        Route::get('/tambah',[ContactController::class,'create']);
        Route::post('/',[ContactController::class,'store']);
        Route::put('/{id}',[ContactController::class,'update']);
        Route::delete('/{id}',[ContactController::class,'destroy']);
    });
});

Route::middleware(['auth'])->group(function(){
    Route::get('/dashboard', 'HomeController@index')->name('home');
    

    // Route::prefix('pejabat')->group(function() 
    // {
    //     Route::get('/',[PejabatController::class,'index']);
    // });

    Route::prefix('ajuan')->group(function() 
    {
        Route::get('/',[AjuanController::class,'index']);
        Route::get('/tambah',[AjuanController::class,'create']);
        Route::post('/',[AjuanController::class,'store']);
        Route::put('/{id}',[AjuanController::class,'update']);
        Route::delete('/{id}',[AjuanController::class,'destroy']);
    });

    // Route::prefix('contact')->group(function() 
    // {
    //     Route::get('/',[ContactController::class,'index']);
    // });
});